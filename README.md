# icons
An icon previewer of all [go-icon](https://gitea.com/go-icon) SVGs.

# Usage
```text
> go get -u gitea.com/go-icon/icons
> icons --help
```
icons runs on port `8000` by default, but can be changed with the `--port` flag.