module gitea.com/go-icon/icons

go 1.13

require (
	gitea.com/go-icon/feather v0.0.0-20191209031537-30eb9b5e065a
	gitea.com/go-icon/fontawesome v0.0.0-20191209021645-316c47069c4b
	gitea.com/go-icon/iconic v0.0.0-20191208211025-9679b60b302e
	gitea.com/go-icon/ionicon v0.0.0-20191208211119-879ffe62fcfe
	gitea.com/go-icon/octicon v0.0.0-20191208210904-8b8d4464c91b
	gitea.com/go-icon/sprite v0.0.0-20191209030055-6e9173dd2316
)
