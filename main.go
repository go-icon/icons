package main

import (
	"flag"
	"fmt"
	"gitea.com/go-icon/feather"
	"gitea.com/go-icon/fontawesome"
	"gitea.com/go-icon/iconic"
	"gitea.com/go-icon/ionicon"
	"gitea.com/go-icon/octicon"
	"gitea.com/go-icon/sprite"
	"html/template"
	"net/http"
)

var tpl = `
<html>
<head>
<title>{{.Name}}</title>
</head>
<body>
` + header + `
<h1>{{.Name}}</h1>
{{range $xlink := .Icons}}
	{{XLink $xlink}}
{{end}}
</body>
</html>
`

var header = `
<p style="text-align: center;"><a href="/feather">Feather</a> | <a href="/fontawesome">Font Awesome Free</a> | <a href="/iconic">Open Iconic</a> | <a href="/ionicon">Ionicon</a> | <a href="/octicon">Octicon</a></p>
`

var portFlag int

func init() {
	flag.IntVar(&portFlag, "port", 8000, "The port to run icons on")
	flag.IntVar(&portFlag, "p", 8000, "The port to run icons on (shorthand)")
}

func main() {
	flag.Parse()

	http.Handle("/", http.RedirectHandler("/feather", http.StatusPermanentRedirect))

	Feather()
	Fontawesome()
	Iconic()
	Ionicon()
	Octicon()

	http.ListenAndServe(fmt.Sprintf(":%d", portFlag), nil)
}

func Feather() {
	m := sprite.New()
	m.URL = "/svg/feather"

	for _, name := range feather.Icons {
		m.Add(name, feather.Icon(name))
	}

	if err := m.Parse(); err != nil {
		fmt.Printf("feather: %v\n", err)
	}
	http.HandleFunc("/svg/feather", func(writer http.ResponseWriter, request *http.Request) {

		writer.Header().Set("Content-Type", "image/svg+xml")
		writer.Write([]byte(m.XML()))
	})
	http.HandleFunc("/feather", func(writer http.ResponseWriter, request *http.Request) {
		tmpl := template.Must(template.New("feather").Funcs(template.FuncMap{
			"XLink": func(icon string) template.HTML {
				xlink := m.XLink(icon)
				xlink.Style = "width: 64px; height: 64px;"
				return xlink.HTML()
			},
		}).Parse(tpl))
		tmpl.Execute(writer, map[string]interface{}{
			"Name":  "Feather",
			"Icons": feather.Icons,
		})
	})
}

func Fontawesome() {
	m := sprite.New()
	m.URL = "/svg/fontawesome"

	for _, name := range fontawesome.Icons {
		m.Add(name, fontawesome.Icon(name))
	}

	if err := m.Parse(); err != nil {
		fmt.Printf("fontawesome: %v\n", err)
	}
	http.HandleFunc("/svg/fontawesome", func(writer http.ResponseWriter, request *http.Request) {

		writer.Header().Set("Content-Type", "image/svg+xml")
		writer.Write([]byte(m.XML()))
	})
	http.HandleFunc("/fontawesome", func(writer http.ResponseWriter, request *http.Request) {
		tmpl := template.Must(template.New("fontawesome").Funcs(template.FuncMap{
			"XLink": func(icon string) template.HTML {
				xlink := m.XLink(icon)
				xlink.Style = "width: 64px; height: 64px;"
				return xlink.HTML()
			},
		}).Parse(tpl))
		tmpl.Execute(writer, map[string]interface{}{
			"Name":  "Font Awesome Free",
			"Icons": fontawesome.Icons,
		})
	})
}

func Iconic() {
	m := sprite.New()
	m.URL = "/svg/iconic"

	for _, name := range iconic.Icons {
		m.Add(name, iconic.Icon(name))
	}

	if err := m.Parse(); err != nil {
		fmt.Printf("iconic: %v\n", err)
	}
	http.HandleFunc("/svg/iconic", func(writer http.ResponseWriter, request *http.Request) {

		writer.Header().Set("Content-Type", "image/svg+xml")
		writer.Write([]byte(m.XML()))
	})
	http.HandleFunc("/iconic", func(writer http.ResponseWriter, request *http.Request) {
		tmpl := template.Must(template.New("iconic").Funcs(template.FuncMap{
			"XLink": func(icon string) template.HTML {
				xlink := m.XLink(icon)
				xlink.Style = "width: 64px; height: 64px;"
				return xlink.HTML()
			},
		}).Parse(tpl))
		tmpl.Execute(writer, map[string]interface{}{
			"Name":  "Open Iconic",
			"Icons": iconic.Icons,
		})
	})
}

func Ionicon() {
	m := sprite.New()
	m.URL = "/svg/ionicon"

	for _, name := range ionicon.Icons {
		m.Add(name, ionicon.Icon(name))
	}

	if err := m.Parse(); err != nil {
		fmt.Printf("ionicon: %v\n", err)
	}
	http.HandleFunc("/svg/ionicon", func(writer http.ResponseWriter, request *http.Request) {

		writer.Header().Set("Content-Type", "image/svg+xml")
		writer.Write([]byte(m.XML()))
	})
	http.HandleFunc("/ionicon", func(writer http.ResponseWriter, request *http.Request) {
		tmpl := template.Must(template.New("ionicon").Funcs(template.FuncMap{
			"XLink": func(icon string) template.HTML {
				xlink := m.XLink(icon)
				xlink.Style = "width: 64px; height: 64px;"
				return xlink.HTML()
			},
		}).Parse(tpl))
		tmpl.Execute(writer, map[string]interface{}{
			"Name":  "Ionicon",
			"Icons": ionicon.Icons,
		})
	})
}

func Octicon() {
	m := sprite.New()
	m.URL = "/svg/octicon"

	for _, name := range octicon.Icons {
		m.Add(name, octicon.Icon(name))
	}

	if err := m.Parse(); err != nil {
		fmt.Printf("octicon: %v\n", err)
	}
	http.HandleFunc("/svg/octicon", func(writer http.ResponseWriter, request *http.Request) {

		writer.Header().Set("Content-Type", "image/svg+xml")
		writer.Write([]byte(m.XML()))
	})
	http.HandleFunc("/octicon", func(writer http.ResponseWriter, request *http.Request) {
		tmpl := template.Must(template.New("octicon").Funcs(template.FuncMap{
			"XLink": func(icon string) template.HTML {
				xlink := m.XLink(icon)
				xlink.Style = "width: 64px; height: 64px;"
				return xlink.HTML()
			},
		}).Parse(tpl))
		tmpl.Execute(writer, map[string]interface{}{
			"Name":  "Octicon",
			"Icons": octicon.Icons,
		})
	})
}
